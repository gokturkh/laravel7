<?php

namespace App\DataTables;

use App\User;
use Yajra\DataTables\Contracts\DataTableButtons;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class UsersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param  mixed  $query  Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        /*$query = User::query()
            ->select(['id', 'name', 'email'])
            ->where('id', 1);*/

        $query = User::query();

        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->rawColumns(['action', 'test'])
            ->addColumn('action', function ($query) {
                return '<button type="button" class="btn btn-outline-primary btn-sm" id="'.$query->id.'"><i class="fa fa-pencil-square-o"></i></button> '.
                    '<button type="button" class="btn btn-outline-danger btn-sm" id="'.$query->id.'"><i class="fa fa-trash-o"></i></button>';
                //return '<button type="button" id="'.$query->id.'" class="btn btn-outline-primary btn-sm">Primary</button>';
            });/*->addColumn('test', function ($query) {
                return '<button type="button" class="btn btn-outline-primary btn-sm" id="'.$query->id.'"><i class="fa fa-trash-o"></i></button>';
                //return '<button type="button" id="'.$query->id.'" class="btn btn-outline-primary btn-sm">Primary</button>';
            });*/
    }

    /**
     * Get query source of dataTable.
     *
     * @param  \App\User  $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('users-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom("<'row'<'col-sm-12 mb-4'B>><'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>")
            ->orderBy(1, 'asc')
            //->paging(false)
            ->parameters([
                'orderClasses' => false,
                //'processing' => false,
                'pagingType' => 'first_last_numbers',
                'renderer' => [
                    'header' => 'bootstrap4',
                    'pageButton' => 'bootstrap',
                ],

                // Buttons default class
                'buttons' => [
                    'buttons' => [
                        [
                            'extend' => 'excel', 'text' => 'EXCEL'
                        ],
                        [
                            'extend' => 'print', 'text' => 'PRINT'
                        ],
                        [
                            'extend' => 'reload', 'text' => 'RELOAD'
                        ],
                        [
                            'extend' => 'reset', 'text' => 'RESET'
                        ],
                    ],
                    'dom' => [
                        'button' => [
                            'className' => 'btn btn-outline-primary btn-sm',
                        ]
                    ]
                ]
            ])
            ->stripeClasses(['alert-info', 'alert-light']);
        /*->buttons(
            Button::raw([
                'extend' => 'excel',
                'text' => 'deneme'
            ]),

        Button::make('create')->text('Oluştur'),
        Button::make('export'),
        Button::make('print'),
        Button::make('reset'),
        Button::make('reload')
        );*/
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('DT_RowIndex')->title('#')->exportable(false),
            Column::make('id'),
            Column::make('name'),
            Column::make('email')->content('<button type="button" class="btn btn-outline-danger btn-sm">YOK</button>'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(65)
                ->addClass('text-center')
                ->title(''),
            /*Column::computed('test')
                ->exportable(false)
                ->printable(false)
                ->width(45)
                ->addClass('text-center')
                ->title('')*/
            /*Column::make('created_at'),
            Column::make('updated_at'),*/
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Users_'.date('YmdHis');
    }
}
