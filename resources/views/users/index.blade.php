@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <!--<div class="card-header">
                        <button type="button" class="btn btn-outline-secondary" id="excel"><i
                                    class="fa fa-file-excel-o"></i></button>
                        <button type="button" class="btn btn-outline-secondary" id="print"><i class="fa fa-print"></i>
                        </button>
                        <button type="button" class="btn btn-outline-secondary" id="refresh"><i
                                    class="fa fa-refresh"></i></button>
                        <button type="button" class="btn btn-outline-secondary" id="reload"><i class="fa fa-undo"></i>
                        </button>
                    </div>-->

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        {{$dataTable->table(['class' => 'table table-bordered'], false)}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    {{$dataTable->scripts()}}

    <script>
        /*$(document).ready(function () {
            var table = $('#users-table').DataTable();

            $("#excel").on("click", function () {
                table.ajax.reload();
                //table.draw();
            });
        });*/
    </script>
@endpush